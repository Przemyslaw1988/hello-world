package pl.codementors.helloworld;

import java.util.Scanner;

/**
 * Welcome class.
 * @author psysiu
 */
public class HelloWorld {

    /**
     * Welcome method.
     * @param args Application starting parameters.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        
        byte byteVar = 1;
        short shortVar = 1;
        int intVar = 1;
        long longVar = 1;
        float floatVar = 1;
        double doubleVar = 1;
        boolean booleanVar = true;
        char charVar = 'a';
        
        
        Scanner inputScanner = new Scanner(System.in);
        byteVar = inputScanner.nextByte();
	shortVar = inputScanner.nextShort();
	intVar = inputScanner.nextInt();
	longVar = inputScanner.nextLong();
	floatVar = inputScanner.nextFloat();
	doubleVar = inputScanner.nextDouble();
	booleanVar = inputScanner.nextBoolean();
        System.out.println(byteVar);
	System.out.println(shortVar);
	System.out.println(intVar);
	System.out.println(longVar);
	System.out.println(floatVar);
	System.out.println(doubleVar);
	System.out.println(booleanVar);
    }
}

